import {mount} from '@vue/test-utils'
import App from '@/App.vue'

const wrapper = mount(App);
const vm = wrapper.vm;

describe('App.vue', () => {
    it('should render correct contents', () => {
        const hello = vm.$el.querySelector('.hello h1')
        expect(hello).not.toEqual(null)
        expect(hello!.textContent)
            .toEqual('Welcome to Your Vue.js + TypeScript App')
    })
});
